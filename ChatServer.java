package nmmu.wrap302.l09.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChatServer {
    // a class encapsulating a message that will be sent to all clients, what a client "said"
    public class Message {
        String user;
        String message;

        public Message(String user, String message) {
            this.user = user;
            this.message = message;
        }
    }

    // a SHARED data structure used for obtaining the connection of a particular client using
    // the handle (name) of the user
    protected HashMap<String, ClientConnection> connections;

    // because the connections data-structure is SHARED/USED by multiple threads, race conditions
    // can occur, therefore a lock is needed
    protected Lock connectionLock = new ReentrantLock();

    /**
     * Add a connection to the connections map
     *
     * @param userHandle The handle of the connection's user
     * @param connection The connection dealing with communicating with the user
     */
    public void addUser(String userHandle, ClientConnection connection) {
        connectionLock.lock();
        try {
            connections.put(userHandle, connection);
            System.out.println("Connection stored: " + userHandle);

        } catch (Exception e) {
            System.out.println("Unable to store connection...");

        } finally {
            connectionLock.unlock();
        }
    }

    /**
     * Terminate the connection with the given user
     *
     * @param userHandle The handle of the user whose connection is to be terminated
     */
    public void removeUser(String userHandle) {
        connectionLock.lock();
        try {
            connections.remove(userHandle);
            System.out.println("Connection removed: " + userHandle);

        } catch (Exception e) {
            System.out.println("Unable to remove connection...");

        } finally {
            connectionLock.unlock();
        }
    }

    /**
     * Send a message to all connected users.
     *
     * @param userHandle The handle of the user sending the message.
     * @param message    The message to be sent
     */
    public void broadcastMessage(String userHandle, String message) {
        connectionLock.lock();
        try {
            System.out.println("Broadcast: " + userHandle + " >> " + message);
            for (ClientConnection connection : connections.values()) {
                connection.sendMessage(userHandle, message);
            }
        } finally {
            connectionLock.unlock();
        }
    }

    /**
     * Query for the list of user handles of connected users.
     *
     * @return The handles of connected users.
     */
    public Collection<String> getUsers() {
        connectionLock.lock();
        try {
            return connections.keySet();

        } catch (Exception e) {
            System.out.println("Unable to return users...");

        } finally {
            connectionLock.unlock();
        }

        return null;
    }

    public static void main(String[] args) {
        new ChatServer();
    }

    public ChatServer() {
        // set up data structures
        connections = new HashMap<String, ClientConnection>();

        try {
            // start server
            System.out.println("Starting Server");
            ServerSocket server = new ServerSocket(5050);

            // accept requests from clients to connect
            while (true) {
                // wait for client to connect
                Socket socket = server.accept();
                System.out.println("New Connection accepted");

                // create new communication thread per client
                ClientConnection connection = new ClientConnection(this, socket);

                // start communication with client
                connection.start();
            }
        } catch (IOException e) {
            // something went wrong!
            e.printStackTrace();
        }
    }
}