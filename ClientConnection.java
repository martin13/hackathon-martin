package nmmu.wrap302.l09.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ClientConnection extends Thread {
    // reference to server object connected too
    private ChatServer server;

    // the communication channels through which the server and THIS client communicate
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    // because multiple threads could be sending output to THIS client's user at the same
    // time, a lock is needed to control the communication from the server to the client to
    // prevent messages being garbled (race conditions)
    private Lock outLock = new ReentrantLock();

    // the handle of this connection's userHandle
    private String userHandle;

    // constants of the messages this particular server & client know how to user
    private static final String MESSAGE_COMMAND = "#MESSAGE";
    private static final String USERS_COMMAND = "#USERS";
    private static final String QUIT_COMMAND = "#QUIT";

    /**
     * Set up this connection
     *
     * @param server Server object client is communicating with.
     * @param socket The communication channel used by the server and THIS client.
     */
    public ClientConnection(ChatServer server, Socket socket) {
        // store references
        this.server = server;
        this.socket = socket;
    }

    /**
     * Send a message from a particular user to THIS client's user.
     *
     * @param userHandle The handle of the user sending the message.
     * @param message    The message being sent.
     */
    public void sendMessage(String userHandle, String message) {
        outLock.lock();
        try {
            out.writeUTF(MESSAGE_COMMAND);
            out.writeUTF(userHandle);
            out.writeUTF(message);
            out.flush();

        } catch (IOException e) {
            // oops!
            e.printStackTrace();
        } finally {
            outLock.unlock();
        }
    }

    /**
     * Send all the handles of connected users to THIS connection's client
     *
     * @param users The list of connected users.
     */
    public void sendUsers(Collection<String> users) {
        outLock.lock();
        try {
            out.writeUTF(USERS_COMMAND);
            out.writeInt(users.size());
            for (String user : users) {
                out.writeUTF(user);
            }
            out.flush();

        } catch (IOException e) {
            // something went wrong
            e.printStackTrace();

        } finally {
            outLock.unlock();
        }
    }

    @Override
    public void run() {
        try {
            // obtain i/o streams
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

            // obtain userHandle name
            userHandle = in.readUTF().trim();
            server.broadcastMessage("SERVER", userHandle + " has joined.");

            // add to server structure so can start receiving messages
            server.addUser(userHandle, this);

            // go into input stream polling loop
            String command = "";
            while (!command.equals(QUIT_COMMAND)) {
                // obtain a message from the client
                command = in.readUTF();

                // process messages
                switch (command) {
                    case MESSAGE_COMMAND:
                        // handle message received from userHandle, message expects a
                        // parameter, i.e. the message that was sent, so read it
                        String message = in.readUTF();

                        // tell server to inform all users of the new message
                        server.broadcastMessage(userHandle, message);
                        break;

                    case USERS_COMMAND:
                        // handle request to view users from userHandle
                        sendUsers(server.getUsers());
                        break;

                    case QUIT_COMMAND:
                        server.broadcastMessage("SERVER", userHandle + " has left.");
                        break;

                    default:
                        System.out.printf("Unknown command '%s' received from client with handle '%s'/n", command, userHandle);
                }
            }

            // a QUIT_COMMAND was received - but is handled by the finally block

        } catch (IOException e) {
            // something went wrong, e.g. lost connection
            e.printStackTrace();

        } finally {
            // disconnect, start by closing streams
            try {
                if (in != null)
                    in.close();
                if (out != null)
                    out.close();
                socket.close();
            } catch (IOException e) {
                // error closing connection
                e.printStackTrace();
            }

            // get server to remove from structures
            server.removeUser(userHandle);
        }
    }
}
