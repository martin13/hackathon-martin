package nmmu.wrap302.l09.javaclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {
    // address of server machine
    private static final String ip = "10.111.1.36";

    // messages used by the client/server's protocol
    private static final String MESSAGE_COMMAND = "#MESSAGE";
    private static final String USERS_COMMAND = "#USERS";
    private static final String QUIT_COMMAND = "#QUIT";

    public static void main(String[] args) {
        new Main();
    }

    // the communication channel
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    // must the connection shut down?
    private boolean shutdown = false;

    public void shutdown() {
        shutdown = true;
    }

    public Main() {
        if (connect()) {
            // able to establish connection

            // start thread that receives messages from server
            ServerListenerThread listener = new ServerListenerThread(this, in);
            listener.start();

            // interact with client, sending commands to server until
            // QUIT_COMMAND received
            run();

            System.out.println("Shutting down...");
        } else {
            // could not connect
            System.out
                    .println("Could not establish connection, shutting down...");
        }

    }

    // attempts to connect to chat server and returns true if could, false
    // otherwise
    public boolean connect() {
        try {
            // attempt to connect to server
            socket = new Socket(ip, 5050);

            // obtain streams
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

        } catch (UnknownHostException e) {
            System.out.println("ERROR: Unknown Host - " + e.getMessage());
            return false;

        } catch (IOException e) {
            System.out.println("ERROR: IOException - " + e.getMessage());
            return false;
        }

        return true;
    }

    public void run() {
        try {
            // used for obtaining user input on the console stream
            Scanner sc = new Scanner(System.in);

            // ask user for handle to use
            System.out.print("Enter your handle: ");
            String handle = sc.nextLine();

            // tell server the user's handle
            out.writeUTF(handle);

            // loop that communicates with server until shutting down
            while (!shutdown) {
                // obtain command from user
                System.out.print("> ");
                String command = sc.nextLine();

                // do something depending on command
                switch (command) {
                    case USERS_COMMAND:
                        // want to obtain a list of users on the server
                        out.writeUTF(USERS_COMMAND);
                        out.flush();
                        break;

                    case QUIT_COMMAND:
                        // user wants to disconnect from server
                        out.writeUTF(QUIT_COMMAND);
                        out.flush();
                        shutdown();
                        break;

                    default:
                        // user has typed a message that they want to have sent to other users
                        out.writeUTF(MESSAGE_COMMAND);
                        out.writeUTF(command);
                        out.flush();
                }
            }
        } catch (IOException e) {
            System.out.println("ERROR: IOException - " + e.getMessage());
        }
    }
}
