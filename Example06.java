package nmmu.wrap301.l05;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Example06 {
    public class Person {
        public String name;
        public Integer age;

        public Person(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return String.format("<%s, %s>", name, age);
        }
    }

    public class PersonComparator implements Comparator<Person> {
        @Override
        public int compare(Person o1, Person o2) {
            if(o1.age == o2.age)
                return 0;
            else if(o1.age < o2.age)
                return -1;
            else
                return 1;
        }
    }

    public Example06() {
        // instantiate a set collection, not use of interface
        List<Person> people = new LinkedList<Person>();

        people.add(new Person("Susan", 15));
        people.add(new Person("Samantha", 18));
        people.add(new Person("Jimmy", 19));
        people.add(new Person("John", 21));
        people.add(new Person("Suzy", 18));
        people.add(new Person("Leslie", 18));

        System.out.println("People = " + people);

        filterExample(people);
        mapExample(people);
        minExample(people);
        countExample(people);
        reduceExample(people);
    }

    private void filterExample(List<Person> people) {
        // obtain a stream
        Stream<Person> stream = people.stream();

        // configure stream processing
        // select all person objects whose name starts with an S
        Stream<Person> filteredStream = stream.filter(person -> person.name.startsWith("S"));

        // initiate processing - return result into a List
        List<Person> filteredPeople = filteredStream.collect(Collectors.toList());

        // display
        System.out.println("Filtered by name = " + filteredPeople);

        // OR as single line
        filteredPeople = people.stream()
                .filter(person -> person.age > 20)
                .filter(person -> person.name.startsWith("J"))
                .collect(Collectors.toList());
        System.out.println("Filtered by name and age = " + filteredPeople);
    }

    private void mapExample(List<Person> people) {
        List<String> names = people.stream() // stream of Person objects
                .map(person -> person.name) // now a stream of Strings
                .collect(Collectors.toList());

        System.out.println("Mapped stream = " + names);
    }

    private void minExample(List<Person> people) {
        // find minimum person using the person comparator
        Person youngest = people.stream()
                .min(new PersonComparator())
                .get();
        System.out.println("Youngest person = " + youngest);

        // custom in-line defined comparator
        String shortestName = people.stream()
                .min(Comparator.comparing(person -> person.name.length())) // specifies value to do comparison on
                .map(person -> person.name) // extract name of person, i.e. now a stream of Strings
                .get();
        System.out.println("Shortest name = " + shortestName);
    }

    private void countExample(List<Person> people) {
        long num = people.stream()
                .filter(person -> person.name.length() <= 5)
                .count();

        System.out.println("Num people with names 5 characters or shorter = " + num);
    }

    private void reduceExample(List<Person> people) {
        Integer ages = people.stream() // stream of Person
                .map(person -> person.age) // stream of Integer (age)
                .reduce(0, (sum, age) -> sum + age); // initial value, then how to modify

        System.out.println("Sum of ages = " + ages);
    }

    public static void main(String[] args) {
        new Example06();
    }
}
