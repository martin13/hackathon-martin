package nmmu.wrap302.l09.javaclient;

import java.io.DataInputStream;
import java.io.IOException;

// thread that listens to the server for responses to commands or simply for receiving messages from other users
public class ServerListenerThread extends Thread {
    // reference to main application and communication channel's input stream
    private Main main;
    private DataInputStream in;

    // messages used for communicating with the server
    private static final String MESSAGE_COMMAND = "#MESSAGE";
    private static final String USERS_COMMAND = "#USERS";
    private static final String QUIT_COMMAND = "#QUIT";

    // setup
    public ServerListenerThread(Main main, DataInputStream in) {
        this.main = main;
        this.in = in;
    }

    @Override
    public void run() {
        try {
            boolean shutdown = false;

            // while not instructed by the server to shut down
            while (!shutdown) {
                // read command string from server
                String command = in.readUTF();

                // do something based on command
                switch (command) {
                    case MESSAGE_COMMAND:
                        // next string will be the user name followed by another
                        // string, which will be the message
                        String userHandle = in.readUTF();
                        String message = in.readUTF();

                        // display message
                        System.out.println();
                        System.out.println(userHandle + ": " + message);
                        break;

                    case USERS_COMMAND:
                        // result from a user request issued by the user. First a
                        // number will be sent indicating the number of users,
                        // followed by that many strings of user names
                        int numUsers = in.readInt();

                        System.out.println("Users (" + numUsers + ")");

                        for (int i = 0; i < numUsers; i++) {
                            userHandle = in.readUTF();
                            System.out.println((i + 1) + ") " + userHandle);
                        }
                        break;

                    case QUIT_COMMAND:
                        // server has indicated that the connection must be severed
                        System.out.println("Quitting...");
                        shutdown = true;
                        main.shutdown();
                        break;

                    default:
                        System.out.println("Unknown command received from server: " + command);
                }

            }
        } catch (IOException e) {
            System.out.println("ERROR (ServerListenerThread): IOException - " + e.getMessage());

            // if connection lost or was terminated
            main.shutdown();
        }
    }
}
